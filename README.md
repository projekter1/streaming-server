# Hai-End Streaming - Server

[![pipeline status](https://gitlab.com/hai-end-streaming/streaming-server/badges/main/pipeline.svg)](https://gitlab.com/hai-end-streaming/streaming-server/-/commits/main)
[![Crowdin](https://badges.crowdin.net/hai-end-streaming/localized.svg)](https://crowdin.com/project/hai-end-streaming)
[![License](https://img.shields.io/badge/license-GNU%20AGPL%20v3-blue.svg)](LICENSE)

This is the server of the Hai-End Streaming Project.

For further information please look at the READMEs in

* [backend/README.md](backend/README.md)
* [webui/README.md](webui/README.md)

and at

* our website <https://hai-end-streaming.de/>
* Gitlab <https://gitlab.com/hai-end-streaming/streaming-server>

## Contribution

* See [CONTRIBUTING](CONTRIBUTING.md)
* For translation support: [translate.hi-end-streaming.de](https://translate.hi-end-streaming.de)

## License

[LICENSE](LICENSE)
