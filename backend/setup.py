import setuptools
from app.version import STREAMING_SERVER_VERSION

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="streaming_server",
    version=STREAMING_SERVER_VERSION,
    author="Maik Ender",
    author_email="maik@hai-end-streaming.de",
    description="the easy streaming server",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://hai-end-streaming.de/",
    packages=setuptools.find_packages(),
    # packages=["streaming-server"],
    # py_modules=['streaming-server'],
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: AGPLv3 License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    entry_points={
        "console_scripts": [
            "streaming-server = app.server:main",
        ]
    },
)
