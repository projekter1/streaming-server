from fastapi.testclient import TestClient
import time
import pytest

from app.main_director.media_states import MediaStates


@pytest.fixture
def client(tmp_path):
    MOCK_CONFIG = """\
PortalDirector:
  cloud_proxy: http://example.com:8080
  keypress_url: http://example.net/internal/device/keypress
StreamingDirector:
  streaming_key: '?psk=barfbarf live: "1"'
  streaming_url: rtmp://eaxmple.org/src
  video_bitrate: 1000000
WebServer:
  web_root: %s
""" % str(
        tmp_path
    )

    from app.settings.settings import global_config

    conf = tmp_path / "config.yml"
    conf.write_text(MOCK_CONFIG)
    global_config.STREAMING_CONFIG_FILE = str(conf)
    global_config.PUBLISH_METRICS = False
    from app.app import app

    with TestClient(app) as c:
        yield c


def run(client, name, url_entry, post=False, payload=None, expected=None):
    if post:
        r = client.post("/api/v2/" + url_entry, params=payload)
    else:
        r = client.get("/api/v2/" + url_entry, params=payload)

    if not expected:
        assert r.status_code == 204
        return

    if isinstance(expected, dict):
        response = r.json()
    elif isinstance(expected, str):
        response = r.text
    else:
        response = r.content
    assert response == expected


@pytest.mark.skip(reason="This is not ready for prime time yet")
def test_tunables(client):
    with client as c:
        # ensure stream is stopped
        run(c, "Stream Start", "streaming/stop", post=True)

        # fmt: off
        # WITHOUT STREAMING
        run(c, "Stream Status", "streaming/status", expected={'status': MediaStates.NULL})

        run(c, "Set Volume", "config", {'set_property': 'volume', 'value': '6'}, True, {'return': 'true'})
        run(c, "Get Volume", "config", {'get_property': 'volume'}, True, {'return': '6'})
        run(c, "Set Volume", "config", {'set_property': 'volume', 'value': '5'}, True, {'return': 'true'})
        run(c, "Get Volume", "config", {'get_property': 'volume'}, True, {'return': '5'})

        run(c, "Set wb_red", "config", {'set_property': 'wb_red', 'value': '1'}, True, {'return': 'true'})
        run(c, "Get wb_red", "config", {'get_property': 'wb_red'}, True, {'return': '1'})
        run(c, "Set wb_red", "config", {'set_property': 'wb_red', 'value': '0'}, True, {'return': 'true'})
        run(c, "Get wb_red", "config", {'get_property': 'wb_red'}, True, {'return': '0'})

        run(c, "Set wb_green", "config", {'set_property': 'wb_green', 'value': '1'}, True, {'return': 'true'})
        run(c, "Get wb_green", "config", {'get_property': 'wb_green'}, True, {'return': '1'})
        run(c, "Set wb_green", "config", {'set_property': 'wb_green', 'value': '0'}, True, {'return': 'true'})
        run(c, "Get wb_green", "config", {'get_property': 'wb_green'}, True, {'return': '0'})

        run(c, "Set wb_blue", "config", {'set_property': 'wb_blue', 'value': '1'}, True, {'return': 'true'})
        run(c, "Get wb_blue", "config", {'get_property': 'wb_blue'}, True, {'return': '1'})
        run(c, "Set wb_blue", "config", {'set_property': 'wb_blue', 'value': '0'}, True, {'return': 'true'})
        run(c, "Get wb_blue", "config", {'get_property': 'wb_blue'}, True, {'return': '0'})

        run(c, "Set video_bitrate", "config", {'set_property': 'video_bitrate', 'value': '1000000'}, True, {'return': 'true'})
        run(c, "Get video_bitrate", "config", {'get_property': 'video_bitrate'}, True, {'return': '1000000'})
        run(c, "Set video_bitrate", "config", {'set_property': 'video_bitrate', 'value': '1000001'}, True, {'return': 'true'})
        run(c, "Get video_bitrate", "config", {'get_property': 'video_bitrate'}, True, {'return': '1000001'})

        run(c, "Stream Status", "config", {'status': ' '}, True, {'status': 'STOPPED'})
        run(c, "Stream Start", "config", {'streaming': 'start'}, True, {'return': 'ok'})

        run(c, "Stream Status", "config", {'status': ' '}, True, {'status': 'PLAYING'})

        run(c, "Set Volume", "config", {'set_property': 'volume', 'value': '6'}, True, {'return': None})
        run(c, "Get Volume", "config", {'get_property': 'volume'}, True, {'return': 6.0})
        run(c, "Set Volume", "config", {'set_property': 'volume', 'value': '5'}, True, {'return': None})
        run(c, "Get Volume", "config", {'get_property': 'volume'}, True, {'return': 5.0})

        run(c, "Set wb_red", "config", {'set_property': 'wb_red', 'value': '1'}, True, {'return': None})
        run(c, "Get wb_red", "config", {'get_property': 'wb_red'}, True, {'return': 1.0})
        run(c, "Set wb_red", "config", {'set_property': 'wb_red', 'value': '0'}, True, {'return': None})
        run(c, "Get wb_red", "config", {'get_property': 'wb_red'}, True, {'return': 0.0})

        run(c, "Set wb_green", "config", {'set_property': 'wb_green', 'value': '1'}, True, {'return': None})
        run(c, "Get wb_green", "config", {'get_property': 'wb_green'}, True, {'return': 1.0})
        run(c, "Set wb_green", "config", {'set_property': 'wb_green', 'value': '0'}, True, {'return': None})
        run(c, "Get wb_green", "config", {'get_property': 'wb_green'}, True, {'return': 0.0})

        run(c, "Set wb_blue", "config", {'set_property': 'wb_blue', 'value': '1'}, True, {'return': None})
        run(c, "Get wb_blue", "config", {'get_property': 'wb_blue'}, True, {'return': 1.0})
        run(c, "Set wb_blue", "config", {'set_property': 'wb_blue', 'value': '0'}, True, {'return': None})
        run(c, "Get wb_blue", "config", {'get_property': 'wb_blue'}, True, {'return': 0.0})

        run(c, "Set video_bitrate", "config", {'set_property': 'video_bitrate', 'value': '1000000'}, True, {'return': None})
        run(c, "Get video_bitrate", "config", {'get_property': 'video_bitrate'}, True, {'return': 1000000})
        run(c, "Set video_bitrate", "config", {'set_property': 'video_bitrate', 'value': '1000001'}, True, {'return': None})
        run(c, "Get video_bitrate", "config", {'get_property': 'video_bitrate'}, True, {'return': 1000001})

        run(c, "Set text_overlay", "config", {'set_property': 'text_overlay', 'value': 'BLA'}, True, {'return': None})
        run(c, "Get text_overlay", "config", {'get_property': 'text_overlay'}, True, {'return': 'BLA'})
        run(c, "Set text_overlay", "config", {'set_property': 'text_overlay', 'value': 'BLUB'}, True, {'return': None})
        run(c, "Get text_overlay", "config", {'get_property': 'text_overlay'}, True, {'return': 'BLUB'})

        run(c, "Stream Stop", "config", {'streaming': 'stop'}, True, {'return': 'ok'})
        # fmt: on


if __name__ == "__main__":
    test_tunables()
