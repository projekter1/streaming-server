import py
import pytest

from app.settings.settings import Settings, global_config

MOCK_CONFIG = """\
PortalDirector:
  cloud_proxy: http://example.com:8080
  keypress_url: http://example.net/internal/device/keypress
StreamingDirector:
  streaming_key: '?psk=barfbarf live: "1"'
  streaming_url: rtmp://eaxmple.org/src
  video_bitrate: 1000000
"""


@pytest.fixture
def settings(tmp_path):
    from app.settings import register_class, unregister_class
    from pydantic import BaseModel

    class TestClass(BaseModel):
        one: int = 0

        class Config:
            validate_assignment = True

    conf = tmp_path / "config.yml"
    conf.write_text(MOCK_CONFIG)
    global_config.STREAMING_CONFIG_FILE = str(conf)
    settings = Settings()
    register_class(TestClass)
    settings.read_config()
    return settings


class TestSettings:
    def test_roundtrip(self, tmp_path):
        conf = tmp_path / "config.yml"
        conf.write_text(MOCK_CONFIG)
        global_config.STREAMING_CONFIG_FILE = str(conf)
        settings = Settings()
        settings.read_config()
        settings.write_config()
        assert conf.read_text() == MOCK_CONFIG

    def test_callback(self, settings):

        guinea_pig = {}

        def callback_set(key, value):
            guinea_pig[key] = value

        settings.add_callback("TestClass", callback_set)
        settings["TestClass.one"] = 3

        assert "one" in guinea_pig
        assert guinea_pig["one"] == 3

    def test_validation(self, settings):
        from pydantic import ValidationError

        with pytest.raises(ValueError):
            settings["TestClass.two"] = "s6"
        with pytest.raises(ValidationError):
            settings["TestClass.one"] = "s6"
