###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

from fastapi import APIRouter, Request, Form, Query
from app.main_director import MainDirectorMessages
import logging

from app.main_director.state_id import StateId
from app.version import STREAMING_SERVER_VERSION

logger = logging.getLogger(__name__)
router = APIRouter()


#########################################
# Get Status
#########################################
@router.get("/status")
async def get_status(request: Request):
    logger.info("APIv1 get status")

    video_device_model, _ = request.app.streaming_director.get_video_device()
    audio_devices = request.app.streaming_director.get_audio_device_names(True)
    audio_selected_device = request.app.settings[
        "StreamingDirector.audio_source_device"
    ]

    return {
        "return": {"rc": 0, "msg": "OK"},
        "status": {
            "version": STREAMING_SERVER_VERSION,
            "stream_status": request.app.main_director.state[StateId.streaming][
                "current"
            ],
            "stream_status_intended": request.app.main_director.state[
                StateId.streaming
            ]["intended"],
            "peripherals_power": False,
            "video_device_model": video_device_model.value,
            "audio_devices": audio_devices,
            "audio_selected_device": audio_selected_device,
            "stream_location": request.app.streaming_director.get_property(
                "rtmp_location"
            ),
        },
    }


#########################################
# Show Message to User
#########################################
@router.post("/show_message")
async def post_show_message(msg: str = Form(...)):
    logger.info(f"Message to display '{msg}'")
    # @todo: to something meaningful
    return {
        "return": {
            "rc": 0,
        }
    }


#########################################
# Start Stream
#########################################
@router.post("/start_stream")
async def post_start_stream(request: Request, url: str = Form(...)):
    # request.app.streaming_director.start_pipeline(url)
    request.app.main_director.send_message(MainDirectorMessages.START_SEND, url)
    return {
        "return": {
            "rc": 0,
        }
    }
    # @todo: error handling


#########################################
# Update Stream
#########################################
@router.post("/update_stream")
async def post_update_stream(request: Request, url: str = Form(...)):
    update = request.app.streaming_director.update_stream_location(url)
    return {
        "return": {
            "rc": 0 if update else 1,
        }
    }
    # @todo: error handling


#########################################
# Stop Stream
#########################################
@router.post("/stop_stream")
async def post_stop_stream(request: Request):
    # request.app.streaming_director.stop_pipeline()
    request.app.main_director.send_message(MainDirectorMessages.STOP_SEND)
    return {
        "return": {
            "rc": 0,
        }
    }


#########################################
# Start Receive
#########################################
@router.post("/start_receive")
async def post_start_receive(request: Request, url: str = Form(...)):
    # request.app.streaming_director.start_pipeline(url, True)
    request.app.main_director.send_message(MainDirectorMessages.START_REVC, url)
    return {
        "return": {
            "rc": 0,
        }
    }
    # @todo: error handling


#########################################
# Update Receive
#########################################
@router.post("/update_receive")
async def post_update_receive(request: Request, url: str = Form(...)):
    update = request.app.streaming_director.update_stream_location(url)
    return {
        "return": {
            "rc": 0 if update else 1,
        }
    }
    # @todo: error handling


#########################################
# Stop Receive
#########################################
@router.post("/stop_receive")
async def post_stop_receive(request: Request):
    # request.app.streaming_director.stop_pipeline()
    request.app.main_director.send_message(MainDirectorMessages.STOP_RECV)
    return {
        "return": {
            "rc": 0,
        }
    }
