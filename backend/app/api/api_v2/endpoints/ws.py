###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

import functools
from fastapi import APIRouter
from fastapi import WebSocket
from fastapi import WebSocketDisconnect
import asyncio
import logging
from typing import List
from app.version import STREAMING_SERVER_VERSION

logger = logging.getLogger(__name__)
router = APIRouter()


class WebSocketConnectionManager:

    def __init__(self):
        self.active_connections: List[WebSocket] = []
        self.app = None

    def set_app(self, app):
        self.app = app
        self.app.web_socket_manager = self

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)
        logger.info(f"ws client {websocket.client.host} connected")
        # await websocket.send_json({
        #     'phase': self.rollout.rolloutPhase.value,
        #     'portal_url': self.rollout.portal_name_request_url
        # })
        await websocket.send_json({"server": {"version": STREAMING_SERVER_VERSION, "imagetype": "production"}})
        self.app.main_director.broadcast_state()

    def disconnect(self, websocket: WebSocket):
        logger.info(f"ws client {websocket.client.host} disconnected")
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def async_broadcast(self, data):
        logger.debug(f"ws broadcast {data}")
        for connection in self.active_connections:
            await connection.send_json(data)

    def broadcast(self, data):
        asyncio.run_coroutine_threadsafe(
            self.async_broadcast(data), self.app.async_loop)


web_socket_manager = WebSocketConnectionManager()


@router.websocket("/")
async def websocket_endpoint(websocket: WebSocket):
    await web_socket_manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
    except WebSocketDisconnect:
        web_socket_manager.disconnect(websocket)
