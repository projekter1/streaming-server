###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################
from typing import Optional  # dependecy python <3.10
from fastapi import APIRouter, Request, Response, Query, File, UploadFile
from pydantic import BaseModel
from pathvalidate import sanitize_filename
from PIL import Image
import logging
import os
import shutil

logger = logging.getLogger(__name__)

router = APIRouter()


##########################################
# FILES: upload
##########################################
@router.post("/file")
async def post_file_upload(
    request: Request, response: Response, file: UploadFile = File(...)
):

    # Todo: use config for path (or move to overlay gen directly)
    image_filename = os.path.join(
        request.app.settings["WebServer.overlay_root"], sanitize_filename(file.filename)
    )
    logger.info(f"upload overlay image {image_filename}")
    try:
        with open(image_filename, "wb+") as dest_file:
            shutil.copyfileobj(file.file, dest_file)

            # from https://github.com/ftarlao/check-media-integrity/blob/master/check_mi.py :)
            img = Image.open(
                dest_file
            )  # open the tmp file before writing it to the correct position
            if img.format != "PNG":  # Todo: we should allow other formats as well
                raise IOError("Image is not a PNG")
            img.verify()  # verify that it is a good image, without decoding it.. quite fast
            img.close()

    except Exception as e:
        try:
            os.remove(image_filename)
        except:
            pass
        response.status_code = 400
        logger.error(f"something went wrong during upload {str(e)}")
        logger.exception(e)
        return {"msg": str(e)}


##########################################
# FILES: delete
##########################################
@router.delete("/file")
async def delete_file_delete(request: Request, response: Response, filename: str):
    filename = sanitize_filename(filename)
    logger.info(f"delete overlay image {filename}")
    if filename == "abendmahl.png" or filename == "schmuck.png":
        response.status_code = 400
        return {"msg": "cant't delete default image abendmahl.png"}

    image_filename = os.path.join(
        request.app.settings["WebServer.overlay_root"], filename
    )
    os.remove(image_filename)


##########################################
# FILES: get all
##########################################
@router.get("/file")
async def post_file_getAll(request: Request):

    pictures = request.app.overlay_generator.get_all_overlay_files()
    return {
        "return": {
            "msg": "OK",
            "data": {
                "pictures": pictures,
            },
        }
    }


##########################################
# Set Hymn
##########################################
@router.post("/hymn")
async def post_show_hymn(
    request: Request, hymn: int = Query(None, ge=1, le=437), verse: Optional[str] = ""
):
    request.app.overlay_generator.generate_hymn(hymn, verse)


##########################################
# Set Message
##########################################
@router.post("/text")
async def post_overlay_text(request: Request, right: str, left: Optional[str] = ""):
    request.app.overlay_generator.overlay_message_left = left
    request.app.overlay_generator.overlay_message_right = right
    request.app.overlay_generator.generate_and_display()


##########################################
# Clear Overlay Text
##########################################
@router.delete("/text")
async def post_clear_text(request: Request):
    request.app.overlay_generator.overlay_message_left = None
    request.app.overlay_generator.overlay_message_right = None
    request.app.overlay_generator.generate_and_display()


##########################################
# Receive Image Path
##########################################
@router.post("/image")
async def post_overlay_image(request: Request, response: Response, path: str):
    overlay_image, err = request.app.overlay_generator.image_overlay_location_sanitize(
        path
    )

    if overlay_image:
        request.app.overlay_generator.overlay_image = overlay_image
        request.app.overlay_generator.generate_and_display()
    else:
        response.status_code = 400
        return {"return": {"rc": 1, "errormsg": err}}


##########################################
# Clear Overlay Image
##########################################
@router.delete("/image")
async def post_clear_image(request: Request):
    request.app.overlay_generator.overlay_image = None
    request.app.overlay_generator.generate_and_display()
