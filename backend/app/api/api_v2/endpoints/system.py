###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

import os
from fastapi import APIRouter
import logging

logger = logging.getLogger(__name__)
router = APIRouter()


#########################################
# Get Status
#########################################
@router.get("/status")
async def get_status():
    logger.info("APIv1 get status")
    return {"msg": "Roter Saft tut Leib und Seele gut!", "imagetype": "production"}


@router.post("/off")
async def post_off():
    logger.info("POWEROFF Requested")
    os.system("sudo shutdown -h 0")


@router.post("/reboot")
async def post_reboot():
    logger.info("REBOOT REQUESTED")
    os.system('sudo reboot "3 tryboot"')


@router.post("/restart")
async def post_restart():
    logger.info("RESTARTING STREAMING SERVER")
    os.system("sudo systemctl restart streaming-server.service")

