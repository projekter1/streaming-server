import os


def relative_to_module(filename):
    dirname = os.path.dirname(__file__)
    return os.path.join(dirname, "..", filename)
