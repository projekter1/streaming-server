import logging
import os


class LoggerRateLimitFilter(logging.Filter):

    def __init__(self, number_test_chars = 30):
        super(LoggerRateLimitFilter, self).__init__()

        self.last_line_start = None
        self.number_of_equal_line = 0
        self.number_test_chars = number_test_chars
        self.level = 0

    def filter(self, record):
        current_line_start = record.getMessage()[0:self.number_test_chars]
        if current_line_start == self.last_line_start:
            # we found an equal line so we suppress it
            self.number_of_equal_line += 1
            return False
        else:
            # current line is not equal
            if self.number_of_equal_line > 0:
                # append number of suppressed lines to message
                #record.msg = f"---- {self.number_of_equal_line} EQUAL STARTING LINES SUPPRESSED ----{os.linesep}{record.getMessage()}"
                __number_of_equal_line = self.number_of_equal_line
                self.number_of_equal_line = 0
                logger = logging.getLogger(record.name).log(self.level, f"---- {__number_of_equal_line} EQUAL STARTING LINES SUPPRESSED ----")
            
            self.level = record.levelno
            self.last_line_start = current_line_start
        
        return True
