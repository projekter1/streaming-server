from enum import Enum, auto


class StateId(Enum):
    streaming = auto()
    voip = auto()
