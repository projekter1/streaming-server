from sqlite3 import register_converter
import threading
import logging
import time
from gi.repository import GLib, GObject
from app.streaming.director import StreamingDirector

# from voip_director import VoIPDirector
import prometheus_client
import app.helper as helper

logger = logging.getLogger(__name__)


# def _main_loop(queue):
#    streaming_director.main_loop(queue)

from .main_director_messages import MainDirectorMessages
from .media_states import MediaStates
from .state_id import StateId


# TODO: test what happens in case of concurent requests (e.g., what happens to the state etc)


class MainDirector:

    # current state of the whole server
    # remeber to change the broadcast function as well
    state = {
        StateId.streaming: {"current": MediaStates.NULL, "intended": MediaStates.NULL},
        StateId.voip: {"current": MediaStates.NULL, "intended": MediaStates.NULL},
    }

    ERROR_INTERVAL = 5 * 60  # 5mins
    ERROR_THRESHOLD = 3
    ERROR_RESTART_WAIT_INTERVAL = 10

    def __init__(self, app):
        self.app = app
        self.app.main_director = self
        self.error_events = []

        if GObject.pygobject_version < (3, 11, 0):
            GObject.threads_init()
        self.streaming_director = StreamingDirector(self.app)
        # self.voip_director = VoIPDirector(self.app)

        self.receive_pipe = False
        self.stream_url = None

        if GObject.pygobject_version >= (2, 16, 0):
            self.glib_loop = GLib.MainLoop()
        else:
            self.glib_loop = GObject.MainLoop()
        self.glib_thread = threading.Thread(target=self.glib_mainloop)
        self.glib_thread.start()

        from app.settings.settings import global_config

        if global_config.PUBLISH_METRICS:
            self.metrics_running_state = prometheus_client.Gauge(
                "streaming_state", "Streaming Status", ["state"]
            )
            self.metrics_running_state.labels("state").set_function(
                lambda: self.state[StateId.streaming]["current"]
            )
            self.metrics_running_state.labels("intended").set_function(
                lambda: self.state[StateId.streaming]["intended"]
            )
            self.metrics_running_state.labels("voip_state").set_function(
                lambda: self.state[StateId.voip]["current"]
            )
            self.metrics_running_state.labels("voip_intended").set_function(
                lambda: self.state[StateId.voip]["intended"]
            )

    def shutdown(self):

        self.stop_glib_mainloop()

    def glib_mainloop(self):

        logger.info("GLIB MainLoop")

        self.glib_loop.run()

        logger.info("run")

    def stop_glib_mainloop(self):
        """
        Call this before exiting; it stops and joins the GLib loop
        created by :func:`start_glib_loop`.
        """
        if self.glib_loop:
            self.glib_loop.quit()
            self.glib_loop = None
        if self.glib_thread:
            self.glib_thread.join()
            self.glib_thread = None

    def send_message(self, message: MainDirectorMessages, *args):
        message_source = helper.caller_module()

        logger.info(f"received message '{message.name}' from {message_source}")
        # TODO: raise error in case of failure
        if message == MainDirectorMessages.STOP:
            self._stop_stream()
        # elif message == MainDirectorMessages.REQUEST_SEND_START:
        # elif message == MainDirectorMessages.REQUEST_RECV_START:
        elif message == MainDirectorMessages.START_SEND:
            logger.debug(f"args: {args}")
            self._stream_start(args[0], False)
        elif message == MainDirectorMessages.START_REVC:
            logger.debug(f"args: {args}")
            self._stream_start(args[0], True)
        elif message == MainDirectorMessages.STOP_SEND:
            self._stop_stream_send()
        elif message == MainDirectorMessages.STOP_RECV:
            self._stop_stream_recv()

        elif message == MainDirectorMessages.VOIP_READY:
            self._set_state_change(StateId.voip, MediaStates.READY)
        elif message == MainDirectorMessages.VOIP_CALL_DIALING:
            self._set_state_change(StateId.voip, MediaStates.STARTING)
        elif message == MainDirectorMessages.VOIP_CALL_ESTABLISHED:
            self._set_state_change(StateId.voip, MediaStates.PLAYING)
        elif message == MainDirectorMessages.VOIP_CALL_STOPPED:
            self._set_state_change(StateId.voip, MediaStates.NULL)

        elif message == MainDirectorMessages.STREAM_STARTING:
            self._set_state_change(StateId.streaming, MediaStates.STARTING)
        elif message == MainDirectorMessages.STREAM_PLAYING:
            self._set_state_change(StateId.streaming, MediaStates.PLAYING)
            self._start_voip()
        elif message == MainDirectorMessages.STREAM_STOPPED:
            self._set_state_change(StateId.streaming, MediaStates.NULL)

        elif message == MainDirectorMessages.RECOVER:
            self._recover()

        # elif message == MainDirectorMessages.WARNING_QOS:

        elif message == MainDirectorMessages.ERROR:
            self._error_handling_other(message_source, args)
        elif message == MainDirectorMessages.ERROR_NETWORK:
            self._error_handling_network(message_source, args)
        elif message == MainDirectorMessages.ERROR_DEVICE:
            self._error_handling_other(message_source, args)
        elif message == MainDirectorMessages.ERROR_ENCODER:
            self._error_handling_other(message_source, args)
        else:
            logger.error(f"recv unknown message! Message not processed {message}")

    def _set_state_change(self, state_id: StateId, requested_state: MediaStates):
        if state_id not in self.state:
            # todo: raise error
            return False

        logger.info(f"change state {state_id.name} to '{requested_state.name}'")
        self.state[state_id]["current"] = requested_state
        self.broadcast_state()

        return True

    def _request_state_change(self, state_id: StateId, requested_state: MediaStates):

        if state_id not in self.state:
            # todo: raise error
            return False
        logger.info(f"request state change {state_id.name} to '{requested_state.name}'")

        ret = False
        if requested_state == MediaStates.NULL:
            if self.state[state_id]["current"] == MediaStates.RECOVER or self.state[state_id]["current"] == MediaStates.ERROR:
                # if we are in the RECOVER state and a stop requst comes, then
                #  the pipe is already stopped
                # so we only set everything to NULL (everything should be ok)
                # and return False so that the pipe is not stopped twice
                self.state[state_id]["current"] = MediaStates.NULL
                self.state[state_id]["intended"] = MediaStates.NULL
                ret = False
            elif self.state[state_id]["current"] != MediaStates.NULL:
                self.state[state_id]["current"] = MediaStates.NULL
                self.state[state_id]["intended"] = MediaStates.NULL
                ret = True
        elif requested_state == MediaStates.PLAYING:
            if (
                self.state[state_id]["intended"] == MediaStates.NULL
                or self.state[state_id]["current"] == MediaStates.RECOVER
            ):
                self.state[state_id]["intended"] = MediaStates.PLAYING
                ret = True
        elif requested_state == MediaStates.RECOVER:
            if (
                self.state[state_id]["intended"] != MediaStates.NULL
                and self.state[state_id]["intended"] != MediaStates.RECOVER
            ):
                self.state[state_id]["current"] = MediaStates.RECOVER
                ret = True

        self.broadcast_state()
        return ret

    def broadcast_state(self):
        self.app.web_socket_manager.broadcast(
            {
                "state": {
                    StateId.streaming.name: {
                        "current": self.state[StateId.streaming]["current"].name,
                        "intended": self.state[StateId.streaming]["intended"].name,
                        "direction": "receive" if self.receive_pipe else "transmit"
                    },
                    StateId.voip.name: {
                        "current": self.state[StateId.voip]["current"].name,
                        "intended": self.state[StateId.voip]["intended"].name,
                    },
                },
            }
        )

    def _recover(self):
        # test if the pipeline was started before thus we can recover from a crash
        stream_started = self.app.settings["Device.stream_started"]
        receive_pipe = self.app.settings["Device.receive_pipe"]
        if self.app.settings["Device.device_type"] == "spare":
            logger.info("spare device")
            if stream_started == "true":
                message = (
                    MainDirectorMessages.START_REVC
                    if receive_pipe
                    else MainDirectorMessages.START_SEND
                )
                self.send_message(message, None)
        else:
            logger.info("trying to send recover")
            self.app.portal_director.send_recover()

    def _stop_stream_recv(self):
        if self.receive_pipe:
            return self._stop_stream()
        return False

    def _stop_stream_send(self):
        if not self.receive_pipe:
            return self._stop_stream()
        return False

    def _stop_voip(self):
        logger.info("stopping voip")
        # if self._request_state_change(StateId.voip, MediaStates.NULL):
        # self.voip_director.quit()

    def _start_voip(self):
        logger.info("starting voip")
        # if self.app.settings["VoIPCall.voip_activated"]:
        #     if self._request_state_change(StateId.voip, MediaStates.PLAYING):
        #         self.voip_director.start()

    def _stop_stream(self):
        if self._request_state_change(StateId.streaming, MediaStates.NULL):
            if not self.receive_pipe:
                self.app.overlay_generator.generate_end_card()
                time.sleep(2)
            logger.info("stopping voip")
            self._stop_voip()
            self.streaming_director.stop_pipeline()
            if not self.receive_pipe:
                self.app.overlay_generator.clear_overlay()
            self.app.settings["Device.stream_started"] = False
            self._set_state_change(StateId.streaming, MediaStates.NULL)

    def _stream_start(self, stream_url, receive_pipe=False):
        if self._request_state_change(StateId.streaming, MediaStates.PLAYING):
            self.receive_pipe = receive_pipe
            self.stream_url = stream_url

            self.streaming_director.start_pipeline(stream_url, receive_pipe)

            self.app.settings["Device.stream_started"] = True
            self.app.settings["Device.receive_pipe"] = self.receive_pipe

    # differentiate between spare/local PIs and cloud PIs
    # Todo: make it async
    def stop_pipeline_user(self):
        if self.receive_pipe:
            key_pressed = "stop_receive"
        else:
            key_pressed = "stop_stream"

        self.app.portal_director.send_keypress(key_pressed)
        self.send_message(MainDirectorMessages.STOP)

    # differentiate between spare/local PIs and cloud PIs
    def start_pipeline_user(self, receive_pipe, test=False):
        if self.app.settings["Device.device_type"] == "spare":
            if receive_pipe:
                self.send_message(MainDirectorMessages.START_REVC, None)
            else:
                self.send_message(MainDirectorMessages.START_SEND, None)
        else:
            if receive_pipe:
                key_pressed = "start_receive"
            else:
                if test:
                    key_pressed = "start_test"
                else:
                    key_pressed = "start_stream"
            self.app.portal_director.send_keypress(key_pressed)

    def _check_error_threshold_exceed(self):
        self.error_events.append(time.time())
        curr_error_interval = time.time() - self.ERROR_INTERVAL
        if (
            sum(i > curr_error_interval for i in self.error_events)
            > self.ERROR_THRESHOLD
        ):
            return True
        return False

    def _error_handling_other(self, message_source, message):
        if message_source == "streaming":
            logger.info("set stream state to ERROR")
            self._set_state_change(StateId.streaming, MediaStates.ERROR)
        elif message_source == "voip":
            logger.info("set voip state to ERROR")
            self._set_state_change(StateId.voip, MediaStates.ERROR)
        else:
            logger.error(f"unkown source of ERROR: '{message_source}'")

        # if we didn't had too many error we try to restart:
        if not self._check_error_threshold_exceed():
            logger.error("trying to restart streaming")
            # restart

            if self._request_state_change(StateId.streaming, MediaStates.RECOVER):
                logger.info("stopping pipes to recover")
                self._stop_voip()
                self.streaming_director.stop_pipeline()
                time.sleep(self.ERROR_RESTART_WAIT_INTERVAL)
                logger.info("restarting pipes to recover after sleep")
                self._stream_start(self.stream_url, self.receive_pipe)
        else:
            # TODO: Inform user
            logger.error(
                "restart streaming exceed error threshold in the last minutes. Discontinue to restart. STOPPING PIPE"
            )
            # after logging, stop the pipe so we are in a clean state
            self._stop_voip()
            self.streaming_director.stop_pipeline()

    def _error_handling_network(self, message_source, message):
        logger.error(
            f"received network error in {message_source} trying to restart\nerror message: '{message}'"
        )

        if message_source == "streaming":
            # logger.info("set stream state to ERROR")
            # self._set_state_change(StateId.streaming, MediaStates.ERROR)

            if self._request_state_change(StateId.streaming, MediaStates.RECOVER):
                # restart a stream by asking the
                self._stop_voip()
                self.streaming_director.stop_pipeline()
                self._recover()
        elif message_source == "voip":
            logger.info("set voip state to ERROR")
            # self._set_state_change(StateId.voip, MediaStates.ERROR)

            if self._request_state_change(StateId.streaming, MediaStates.RECOVER):
                # restart a stream by asking the
                self._stop_voip()
                self.streaming_director.stop_pipeline()
                self._recover()
        else:
            logger.error(f"unkown source of NETWROK ERROR: '{message_source}'")
