from pydantic import BaseModel, BaseSettings, StrictStr, StrictInt, ValidationError
import yaml


import logging

logger = logging.getLogger(__name__)

# for sideeffect
from . import device
from . import portal
from . import streaming
from . import web_server
from . import voip_call
from . import voip_register
from . import voip_twinkle
from . import overlay


class GlobalConfig(BaseSettings):
    STREAMING_CONFIG_FILE: str = "config.yml"
    LOGGING_CONFIG_FILE: str = "logging.conf"
    API_PREFIX: str = "/api/v2"

    # because of lagacy, we leave it at this address
    PORTAL_PREFIX: str = "/APIv1"

    HTTP_LISTEN_HOST: str = "0.0.0.0"
    HTTP_LISTEN_PORT: int = 8080

    PUBLISH_METRICS: bool = True

    class Config:
        case_sensitive = True
        env_file = ".env"
        env_file_encoding = "utf-8"


global_config = GlobalConfig()


class Settings:
    def __init__(self, config=None, callbacks=None):
        self.config = config or {}
        self.callbacks = callbacks or {}

    def read_config(self):
        from . import settings_classes

        with open(global_config.STREAMING_CONFIG_FILE, "r") as f:
            config = yaml.safe_load(f)

        logger.debug(config)
        for cls in settings_classes():
            config_name = cls.__name__
            logger.info(f"load config for '{config_name}'")
            d = config.get(config_name, {})  # Load missing with defaults
            logger.info(d)
            self.config[config_name] = cls.parse_obj(d)
            logger.debug(self.config[config_name])
        return self

    def write_config(self):
        from . import settings_classes

        logger.debug("write config")
        config_dict = {}
        for cls in settings_classes():
            config_name = cls.__name__
            props = self.config[config_name].dict(exclude_defaults=True)
            if len(props):  # ignore default-only config
                config_dict[config_name] = props
        logger.debug(config_dict)
        with open(global_config.STREAMING_CONFIG_FILE, "w") as f:
            documents = yaml.dump(config_dict, f)
        return self

    def add_callback(self, config_class_name, func):
        """Add a callback to a config class if some value is changed

        Args:
            config_class (ConfigClass): The config class name
            func (function): callback function should be func(changed_key, value)
        """
        logger.debug(f"set callback for {config_class_name}")
        self.callbacks.setdefault(config_class_name, [])
        self.callbacks[config_class_name].append(func)

    def __getitem__(self, key):

        key = key.split(".", 1)

        if len(key) == 1:
            return self.config.get(key[0], None)

        elif len(key) == 2:
            config_class = self.config.get(key[0], None)
            if config_class is not None:
                return getattr(config_class, key[1])
        return None

    def __setitem__(self, key, value):

        key = key.split(".", 1)
        if len(key) != 2:
            raise ValueError("Not a valid key")

        config_class = self.config.get(key[0], None)
        if not config_class:
            raise ValueError("ConfigClass not found")

        # now set the item
        setattr(config_class, key[1], value)
        # get the typed value (as we need to pass it to the cb)
        value_typed = getattr(config_class, key[1])

        # callbacks
        logger.debug("call all callback after set item")
        for cb in self.callbacks.get(key[0], []):
            cb(key[1], value_typed)

        # save config
        self.write_config()
