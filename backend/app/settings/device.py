from pydantic import BaseModel
from . import settings_class


@settings_class
class Device(BaseModel):
    device_type: str = "spare"

    stream_started: bool = False
    receive_pipe: bool = False

    class Config:
        validate_assignment = True
