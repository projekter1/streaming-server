from pydantic import BaseModel
from . import settings_class


@settings_class
class StreamingDirector(BaseModel):
    streaming_url: str = ""
    streaming_key: str = ""

    force_pipe: str = "None"
    ignore_videosources: list = []

    volume: float = 1
    wb_red: float = 1
    wb_green: float = 1
    wb_blue: float = 1
    video_bitrate: int = 1500000
    audio_source_device: str = "auto"
    audio_sink_device: str = "auto"

    class Config:
        validate_assignment = True
