import requests
import logging
import time
import asyncio

logger = logging.getLogger(__name__)

# TODO: try catch
# TODO: use second url / backup


class PortalDirector:
    RETRY_INTERVAL = 10
    TIMEOUT = 10

    def __init__(self, app):
        self.app = app
        self.app.portal_director = self
        self.sending_recover = False

        # configure request for cloud API
        self.cloud_request = requests.Session()
        if self.app.settings["PortalDirector.cloud_proxy"]:
            self.cloud_request.proxies.update(
                {"http": self.app.settings["PortalDirector.cloud_proxy"]}
            )

    def send_keypress(self, key_pressed):
        if self.app.settings["Device.device_type"] == "pi":
            logger.info(f"send keypress to portal {key_pressed}")
            try:
                r = self.cloud_request.post(
                    self.app.settings["PortalDirector.keypress_url"],
                    data={"key": key_pressed},
                    timeout=self.TIMEOUT,
                )

                logger.info(r.json())
            except requests.exceptions.RequestException as e:
                logger.error(f"could not reach portal {e}")

    async def send_recover_task(self):
        if self.app.settings["Device.device_type"] == "pi":
            stream_started = self.app.settings["Device.stream_started"]
            receive_pipe = self.app.settings["Device.receive_pipe"]

            # if send_recover is called multiple times return
            # one request is enough as we inf loop until we got an answer
            if self.sending_recover:
                return
            self.sending_recover = True
            while self.sending_recover:
                logger.info(
                    f"send recover to portal streamStarted: '{stream_started}' receive_pipe: '{receive_pipe}'"
                )
                try:
                    r = self.cloud_request.post(
                        self.app.settings["PortalDirector.keypress_url"],
                        data={
                            "key": "recover",
                            "streamStarted": stream_started,
                            "streamReceived": receive_pipe,
                        },
                        timeout=self.TIMEOUT,
                    )
                    logger.debug(r.json())
                    if r.status_code == 200:
                        logger.info("recover received by portal")
                        self.sending_recover = False
                    else:
                        logger.warn(
                            f"recover not received by portal. Retry in {self.RETRY_INTERVAL}"
                        )
                        time.sleep(self.RETRY_INTERVAL)
                except requests.exceptions.RequestException as e:
                    logger.error(f"could not reach portal {e}")
                    time.sleep(self.RETRY_INTERVAL)

    def send_recover(self):
        asyncio.run_coroutine_threadsafe(
            self.send_recover_task(), self.app.async_loop)
