###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################
# @TODO refactor variable names

from PIL import Image, ImageDraw, ImageFont
import csv
import textwrap
import os
import tempfile
from pathvalidate import sanitize_filename

import json
import logging
import app.helper as helper

logger = logging.getLogger(__name__)


class OverlayGenerator:
    app = None

    overlay_message_left = None
    overlay_message_right = None
    overlay_image = None

    overlay_last_img = None

    def __init__(self, app):
        self.app = app
        self.app.overlay_generator = self

    def wrap_text_dynamically(self, text, text_size):
        """Wrap text to fit into lower third

        Args:
        text (String): Text to wrap

        Returns:
            String: Text with linebreaks
        """
        if not text:
            return ""
        r = ""
        for t in text.splitlines():
            r += "\n".join(textwrap.wrap(t, width=text_size))
            r += "\n"

        return r

    def generate_overlay(self, destination_path):
        """
        Generates an overlay png image based on different messages

        Args:
            destination_path (String): path and filename where the png will be saved to
            mode (String): Set mode for lower third. See parameter msg
            overlay_path (String): path for the overlay image or None in case of no overlay picture is provided.
            msg (String): Message to display, format depends on mode set.
                Mode       *   msg
                -----------------------------------------
                hymn       *   <number>#<verse> (verse is optional, # not)
                dynamic    *   <top left string> # <bottom left String> # <right string>

            dynamic mode can be used if a error message has to be displayed, in case of strange things happen.

        """
        # Variable definitions
        logo_size = 70, 70
        image_size = 1280, 720
        font_path = "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf"
        font_path_bold = "/usr/share/fonts/truetype/liberation/LiberationSans-Bold.ttf"
        logo_file = helper.relative_to_module("overlay/logo.png")

        # Overlay generation
        overlay = Image.new("RGBA", image_size, (255, 255, 255, 0))
        overlay_image = ImageDraw.Draw(overlay)

        # Insert Overlay Picture, if present
        if self.overlay_image:
            background_orig = Image.open(self.overlay_image).convert("RGBA")
            background = background_orig.resize(image_size)
            overlay.paste(background, (0, 0), background)

        """
        Depending on parameter "mode", template for lower third is chosen.
        """
        if self.overlay_message_right:
            # Depending on parameter, we split the messsage.
            # msg_split = msg.split("#")
            # set default values for lower third
            lower_third_1 = 10, 600
            lower_third_2 = 1270, 700
            separator_1 = 280, 610
            separator_2 = 285, 690
            # Set default font for 2 line lower third
            font_right = ImageFont.truetype(font_path, 36)
            font_bold_2 = ImageFont.truetype(font_path_bold, 36)
            # TODO refactor variable names to fit "dynamic"
            """
            prints bible text for divine service
            msg_split[0] is upper line left
            msg_split[1] is upper line low
            msg_split[2] is bible text, line separation by \n
            """

            # get line counts:

            overlay_message_left_wrapped = self.wrap_text_dynamically(
                self.overlay_message_left, 19
            )
            overlay_message_right_wrapped = self.wrap_text_dynamically(
                self.overlay_message_right, 70
            )

            line_count = max(
                len(overlay_message_left_wrapped.splitlines()),
                len(overlay_message_right_wrapped.splitlines()),
            )
            lower_third_y = lower_third_1[1]
            # set font and lower third height depending on lines
            if line_count > 2:
                # 3 lines doesn't change lower third height
                font_right = ImageFont.truetype(font_path, 26)
                # if 4 or more lines, a higher lower third is needed. With 3 lines,
                # lower third is not affected.
                # FIXME: remove magic numbers
                lower_third_y = 600 - ((line_count - 2) * 26)
                separator_y = 610 - ((line_count - 2) * 26)
                lower_third_1 = 10, lower_third_y
                separator_1 = 280, separator_y
            # Draw lower third
            overlay_image.rectangle((lower_third_1, lower_third_2), fill="white")

            if overlay_message_left_wrapped:
                overlay_image.rectangle((separator_1, separator_2), fill="black")

                #  calculate text positions
                width_left, hight_left = overlay_image.textsize(
                    overlay_message_left_wrapped, font=font_bold_2
                )
                if width_left > 260:
                    # if chapter is too broad, decrease font size
                    font_bold_dyn = ImageFont.truetype(font_path_bold, 26)
                    width_left, hight_left = overlay_image.textsize(
                        overlay_message_left_wrapped, font=font_bold_dyn
                    )
                    # wv, hv = overlay_image.textsize(verse, font=font_bold_dyn)
                    overlay_image.multiline_text(
                        ((280 - width_left) / 2, lower_third_y + 10),
                        overlay_message_left_wrapped,
                        font=font_bold_dyn,
                        fill="black",
                    )
                    # overlay_image.text(((280-wv)/2, lower_third_y + 41), verse, font=font_bold_dyn, fill="black")
                else:
                    overlay_image.multiline_text(
                        ((280 - width_left) / 2, lower_third_y + 10),
                        overlay_message_left_wrapped,
                        font=font_bold_2,
                        fill="black",
                    )
                    # overlay_image.text(((280-wv)/2, lower_third_y + 46), verse, font=font_bold_2, fill="black")

                # right side (if left enabled)
                overlay_image.multiline_text(
                    (315, lower_third_y + 10),
                    overlay_message_right_wrapped,
                    font=font_right,
                    fill="black",
                )
            else:
                # right side (if left disabled)
                overlay_image.multiline_text(
                    (40, lower_third_y + 10),
                    overlay_message_right_wrapped,
                    font=font_right,
                    fill="black",
                )

        # print logo in upper left
        logo_image = Image.open(logo_file).convert("RGBA")
        logo_image.thumbnail(logo_size)
        overlay.paste(logo_image, (20, 20), logo_image)
        overlay.save(destination_path, "PNG", compress_level=0)

    def clear_overlay(self):
        self.overlay_message_left = None
        self.overlay_message_right = None
        self.overlay_image = None

    def generate_and_display(self):
        logger.info(
            "Generate Overlay:\n  LeftText:\t {0}\n  RightText:\t {1}\n  Img:\t\t {2}".format(
                self.overlay_message_left,
                self.overlay_message_right,
                self.overlay_image,
            )
        )

        overlay_img_new_tmp = tempfile.NamedTemporaryFile(delete=False)

        self.generate_overlay(overlay_img_new_tmp)
        self.app.streaming_director.set_property(
            "image_overlay_location", overlay_img_new_tmp.name
        )
        if self.overlay_last_img:
            os.unlink(self.overlay_last_img)
        self.overlay_last_img = overlay_img_new_tmp.name

    # Helper functions for API access
    def generate_end_card(self):
        self.overlay_message_left = ""
        self.overlay_message_right = (
            self.app.settings["Overlay.end_card_text"]
        )
        self.generate_and_display()

    def generate_hymn(self, hymn, verse):
        """
        msg is formatted <number>#<verses>, i.e. "123#1-3+5".
        if no verses are given, specify msg as "123#"
        msg_split[0] contains number, msg_split[1] contains verses.
        """

        csv_path = helper.relative_to_module("overlay/hymnlist.de.csv")

        # give variables a better name
        hymn_number_text = hymn
        hymn_verses = verse

        # Read hymn list from csv file
        hymns = csv.reader(open(csv_path, "rt"), delimiter=";")
        hymn_list = []
        hymn_list.extend(hymns)
        names = []
        hymn_number = int(hymn) - 1  # index starts at 0, hymnal index at 1
        for data in hymn_list:
            names.append(data)

        self.overlay_message_left = "Gesangbuch " + str(hymn) + "\n" + str(verse)
        # @FIXME: sanitize max input
        self.overlay_message_right = names[hymn_number][1]  # get hymn name from list

        self.generate_and_display()

    def image_overlay_location_sanitize(self, s):
        ret = os.path.join(
            self.app.settings["WebServer.overlay_root"], sanitize_filename(s)
        )

        try:
            # from https://github.com/ftarlao/check-media-integrity/blob/master/check_mi.py :)
            img = Image.open(ret)  # open the image file
            if (
                img.format != "PNG"
            ):  # TODO: allow JPG and others (maybe not as only pngs should be saved)
                raise IOError("Image is not a PNG")
            img.verify()  # verify that it is a good image, without decoding it.. quite fast
            img.close()
        except Exception as e:
            er = "ERROR: loaded image {0} not valid".format(ret)

            logger.error(er)
            logger.error(str(e))
            ret = None
            return None, er

        return ret, None

    def get_all_overlay_files(self):
        pictures = []
        for root, dirs, files in os.walk(self.app.settings["WebServer.overlay_root"]):
            for filename in files:
                readonly = False
                if filename == "abendmahl.png" or filename == "schmuck.png":
                    readonly = True
                # TODO: only return PNGs and jpegs
                pictures.append({"filename": filename, "readonly": readonly})
        return pictures
