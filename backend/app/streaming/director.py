###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2021,2022,2023 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

import gi

gi.require_version("Gst", "1.0")
from gi.repository import Gst, GLib, GObject

import prometheus_client
import os
import time
import socket
import asyncio
from enum import Enum, auto, IntEnum
import logging
from app.main_director.main_director_messages import MainDirectorMessages
import app.helper as helper

logger = logging.getLogger(__name__)
logger.addFilter(helper.LoggerRateLimitFilter())

# fmt: off
PIPELINE_PARTS = {
    "LIVEAUDIOSRC": "alsasrc name=audio_src",

    "TESTAUDIOSRC": "audiotestsrc wave=ticks",

    "GL": f" queue min-threshold-buffers=1 ! glupload ! glcolorconvert ! gloverlay overlay-width=1280 overlay-height=720 name=image_overlay alpha=1 location={helper.relative_to_module('overlay/default.png')} ! tee name=gl_tee ! glcolorconvert ! gldownload ! video/x-raw,format=YUY2",

    "GL_400": f"queue ! glupload ! glcolorconvert ! gloverlay overlay-height=400 overlay-width=704 name=image_overlay alpha=1 location={helper.relative_to_module('overlay/default.png')} ! tee name=gl_tee ! glcolorconvert ! gldownload ! video/x-raw,format=YUY2",

    "GL_SINK": "gl_tee. ! queue min-threshold-buffers=1 leaky=downstream ! glimagesink sync=false",

    "JANUSUPLOAD": '''vid. ! queue  name=udp_sink_start ! rtph264pay ! udpsink host=127.0.0.1 port=8004 name=udp_sink
        audio. ! queue ! opusenc bitrate=48000 ! rtpopuspay ! udpsink host=127.0.0.1 port=8005 name=udp_sink_audio''',

    "KMSSINK": "videoUncompressed. ! queue ! videoconvert ! kmssink",

    "RTMPSINK": "mux. ! queue2 name=sink_queue max-size-buffers=0 max-size-bytes=26214400 max-size-time=120000000000 use-buffering=true ! rtmpsink name=rtmp_sink",

    "TEXTOVERLAY": "textoverlay valignment=bottom halignment=center line-alignment=left font-desc=\"Sans, 28\" name=txt shaded-background=yes",

    #first queue for CAMLINK 1080: buffers=3 otherwise 1
    "H264FLV": "queue min-threshold-buffers=1 ! v4l2h264enc output-io-mode=4 capture-io-mode=4 extra-controls=\"controls,h264_profile=3,video_bitrate=1500000,h264_i_frame_period=100,video_bitrate_mode=0,repeat_sequence_header=1;\" name=encoder ! video/x-h264,profile=high,level=(string)4 ! tee name=vid ! queue name=rtmp_sink_start ! h264parse ! flvmux name=mux streamable=true latency=500000000 min-upstream-latency=500000000", # metadatacreator=\"Hai-End Streaming Server\"

    #last queue for CAMLINK 1080:           time=500000000 buffer=3
    #last queue for MACROSILICON 1080:      time=100000000 buffer=3
    "AUDIOFILTER": "queue ! audioresample ! audio/x-raw,rate=48000 ! audioconvert ! audio/x-raw,format=S16LE ! level interval=15000000000 peak-ttl=15000000000 name=audiolevel ! volume volume=1.8 name=volume",

    "AUDIOENC": "queue ! tee name=audio ! queue ! audioconvert ! avenc_aac aac-coder=twoloop bitrate=128000 ! queue ! mux.",
}
# fmt: on

#
PIPELINE_DESC_ANALOG = """
    v4l2src name=video_src extra-controls="c,volume=46028" pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=720,height=557,framerate=25/1,interlace-mode=interleaved ! queue ! videoconvert ! deinterlace method=vfir ! queue ! videorate ! video/x-raw,framerate=25/1 ! queue ! videocrop top=79 bottom=78 left=8 right=8 ! queue ! frei0r-filter-white-balance neutral-color-r=1 neutral-color-g=1 neutral-color-b=1 name=wb ! queue ! frei0r-filter-hqdn3d spatial=0.2 temporal=0.2 ! queue ! frei0r-filter-sharpness size=0.1 amount=0.3 ! {GL_400} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)


PIPELINE_DESC_RASPICAM = """
    v4l2src name=video_src ! video/x-raw,width=1280,height=720,framerate=25/1 ! queue ! videorate max-rate=25 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

PIPELINE_DESC_HDMI2CSI = """
    v4l2src name=video_src ! video/x-raw,width=1280,height=720,framerate=25/1,colorimetry=bt601 ! queue ! videorate max-rate=25 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

# YUY2, NV12
PIPELINE_DESC_CAMLINK = """
    v4l2src name=video_src io-mode=4 ! v4l2convert output-io-mode=4 capture-io-mode=4  ! videorate max-rate=25 ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

# Macrosilicon USB 2.0 HDMI Capture Device
# Capable of 720p10 YUV and 720p60, 720p50, 720p30, 720p20, 720p10 MJPEG
# v4l2src name=video_src  ! image/jpeg,width=1280,height=720,framerate=50/1 ! queue ! videorate max-rate=25 ! queue ! jpegdec ! queue ! videoconvert ! queue ! videoscale ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
PIPELINE_DESC_MACROSILICON2109 = """
    v4l2src name=video_src io-mode=4 pixel-aspect-ratio=1/1 ! image/jpeg,width=1280,height=720,framerate=50/1 ! videorate max-rate=25 ! queue ! jpegdec idct-method=0 !  {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

# Logtech PTZ Pro 2
# Capable of 720p30, 720p24, 720p20, 720p15, 720p10, 720p7.5, 720p5
# Sadly neither 720p50 nor 720p25 capable
PIPELINE_DESC_LOGITECHPTZPRO2 = """
    v4l2src name=video_src io-mode=4 pixel-aspect-ratio=1/1 ! image/jpeg,width=1280,height=720,framerate=30/1 ! videorate max-rate=25 ! queue ! jpegdec idct-method=0 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

# Logitech Meetup
# Capable of 720p60, 720p30, ...
# Sadly neither 720p50 nor 720p25 capable
PIPELINE_DESC_LOGITECHMEETUP = """
    v4l2src name=video_src io-mode=4 pixel-aspect-ratio=1/1 ! image/jpeg,width=1280,height=720,framerate=60/1 ! videorate max-rate=25 ! queue ! jpegdec idct-method=0 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

PIPELINE_DESC_ATEMMINIPRO = """
    v4l2src name=video_src  ! image/jpeg ! queue ! videorate max-rate=25 ! queue ! jpegdec ! queue ! videoconvert ! queue ! videoscale ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

PIPELINE_DESC_TEST = """
    videotestsrc is-live=true ! video/x-raw,width=1280,height=720,framerate=25/1 ! {GL} ! {H264FLV}
    {TESTAUDIOSRC} ! {AUDIOFILTER} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
""".format(
    **PIPELINE_PARTS
)

# https://out1.nac-cdn.net/testvideo/720p25/teststream_720mid/index.m3u8
PIPELINE_DESC_RECEIVE = """
    souphttpsrc name=video_src is-live=true location=https://out1.nac-cdn.net/testvideo/720p25/teststream_720mid/index.m3u8 ! hlsdemux name=hls_demux ! queue ! tsdemux name=mux emit-stats=false ! tee name=vid ! queue ! h264parse ! queue min-threshold-buffers=3 ! v4l2h264dec ! queue min-threshold-buffers=3 ! glupload ! glcolorconvert ! glcolorscale ! video/x-raw(memory:GLMemory),width=1280,height=720 ! glimagesinkelement sync=true
    mux. ! queue ! aacparse ! avdec_aac ! tee name=audio ! queue ! audioconvert ! alsasink device=hw:0 name=audio_sink
    vid. ! queue  name=udp_sink_start ! video/x-h264,stream-format=(string)byte-stream,alignment=(string)au ! rtph264pay ! udpsink host=127.0.0.1 port=8004 name=udp_sink
    audio. ! queue ! audioconvert ! opusenc bitrate=48000 ! rtpopuspay ! udpsink host=127.0.0.1 port=8005 name=udp_sink_audio
""".format(
    **PIPELINE_PARTS
)


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix) :]
    return text


def remove_postfix(text, postfix):
    if text.endswith(postfix):
        return text[: -len(postfix)]
    return text


class VideoDeviceModel(Enum):
    TEST = "TEST"
    HAUPPAUGE = "HAUPPAUGE"
    RASPICAM = "RASPICAM"
    CAMLINK = "CAMLINK"
    HDMI2CSI = "HDMI2CSI"
    MACROSILICON2109 = "MACROSILICON2109"
    LOGITECHPTZPRO2 = "LOGITECHPTZPRO2"
    LOGITECHMEETUP = "LOGITECHMEETUP"
    ATEMMINIPRO = "ATEMMINIPRO"


class ErrorStates(IntEnum):
    NULL = 1
    READY = 2
    PAUSED = 3
    PLAYING = 4
    ERROR = 5


class NullGauge:
    def set(*args, **kwargs):
        # gobble
        pass


class StreamingDirector:
    properties = {}

    pipe = None
    receive_pipe = False

    status = "STOPPED"
    # state = ErrorStates.NULL
    # state_intended = ErrorStates.NULL

    METRICS_AUDIOLEVEL_DEFAULT = -90

    def __init__(self, app):
        self.app = app
        self.app.streaming_director = self
        self.app.settings.add_callback("StreamingDirector", self.property_set_callback)

        Gst.init(None)
        Gst.debug_set_active(True)
        Gst.debug_set_default_threshold(1)

        from app.settings.settings import global_config

        if global_config.PUBLISH_METRICS:
            loop = asyncio.get_event_loop()
            task = loop.create_task(self.publish_metrics())

        self.hostname = remove_prefix(socket.gethostname(), "pi-")
        self.fqdn = socket.getfqdn()

        # fmt: off
        self.register_property("video_bitrate",          "extra-controls",  True,  lambda f: self.set_video_bitrate_from_extra_controls(f), lambda s: self.get_video_bitrate_from_extra_controls(s))
        self.register_property("extra_controls",         "extra-controls",  False, None, None)
        self.register_property("volume",                 "volume",          True,  None, None)
        self.register_property("rtmp_location",          "location",        False, None, None)
        self.register_property("wb_red",                 "neutral-color-r", True,  None, None)
        self.register_property("wb_blue",                "neutral-color-b", True,  None, None)
        self.register_property("wb_green",               "neutral-color-g", True,  None, None)
        self.register_property("image_overlay_alpha",    "alpha",           False, None, None)
        self.register_property("image_overlay_location", "location",        False, None, None)
        self.register_property("audio_source_device",    "device",          True,  lambda f: self.get_audio_device_dev_by_name(f, True),   lambda f: self.get_audio_device_name_by_dev(f, True))
        self.register_property("audio_sink_device",      "device",          True,  lambda f: self.get_audio_device_dev_by_name(f, False),  lambda f: self.get_audio_device_name_by_dev(f, False))
        # fmt: on

        self.metrics_audiolevel_channel_count = 1
        self.metrics_txqueue = None
        if global_config.PUBLISH_METRICS:
            # register metrics
            self.metrics_txqueue_bitrate = prometheus_client.Gauge(
                "streaming_bitrate", "Average outgoing bitrate in bits/s"
            )
            self.metrics_txqueue_bytes = prometheus_client.Gauge(
                "streaming_txqueue_bytes",
                "Current amount of data in the transmission queue in bytes"
            )
            self.metrics_txqueue_buffers = prometheus_client.Gauge(
                "streaming_txqueue_buffers",
                "Current amount of buffers in the transmission queue"
            )
            self.metrics_audiolevel = prometheus_client.Gauge(
                "audio_level", "audio level in dB", ['channel', 'type']
            )
            # Provide a reasonable default
            for i in range(self.metrics_audiolevel_channel_count):
                self.metrics_audiolevel.labels(i, 'rms').set(self.METRICS_AUDIOLEVEL_DEFAULT)
                self.metrics_audiolevel.labels(i, 'peak').set(self.METRICS_AUDIOLEVEL_DEFAULT)
                self.metrics_audiolevel.labels(i, 'decay').set(self.METRICS_AUDIOLEVEL_DEFAULT)



            # self.metrics_running_state = prometheus_client.Gauge(
            #     "streaming_state", "Streaming Status", ["state"]
            # )
            # self.metrics_running_state.labels("state").set_function(lambda: self.state)
            # self.metrics_running_state.labels("intended").set_function(
            #     lambda: self.state_intended
            # )

            i = prometheus_client.Info("server", "streaming-server info")

            from app.version import STREAMING_SERVER_VERSION

            i.info({"version": STREAMING_SERVER_VERSION, "imagetype": "production"})

        else:
            self.metrics_txqueue_bitrate = NullGauge()
            self.metrics_txqueue_bytes = NullGauge()
            self.metrics_txqueue_buffers = NullGauge()
            self.metrics_audiolevel = NullGauge()

        # # configure request for cloud API
        # self.cloud_request = requests.Session()
        # if self.app.settings["PortalDirector.cloud_proxy"]:
        #     self.cloud_request.proxies.update(
        #         {"http": self.app.settings["PortalDirector.cloud_proxy"]}
        #     )

        # test if the pipeline was started before thus we can recover from a crash
        # stream_started = self.app.settings["Device.stream_started"]
        # self.receive_pipe = self.app.settings["Device.receive_pipe"]

        # if self.app.settings["Device.device_type"] == "spare":
        #     if stream_started == "true":
        #         self.start_pipeline(receive_pipe=self.receive_pipe)
        # else:
        #     r = self.cloud_request.post(
        #         self.app.settings["PortalDirector.keypress_url"],
        #         data={
        #             "key": "recover",
        #             "streamStarted": stream_started,
        #             "streamReceived": self.receive_pipe,
        #         },
        #     )
        #     logger.info(r.json())

    def get_video_bitrate_from_extra_controls(self, s):
        if s is None:
            return s
        if isinstance(s, str):
            return s
        return s.get_int("video_bitrate").value

    def set_video_bitrate_from_extra_controls(self, s):
        extra_controls = self.get_property("extra_controls")
        if extra_controls == "false":
            return None
        extra_controls.set_value("video_bitrate", int(s))
        logger.info(f"Setting video_bitrate to {int(s)}")

        return extra_controls

    """
    AUDIO DEVICE SECTION
    """

    def get_audio_devices(self, is_src_device: bool):
        # ALSA dependency here
        # Find a suitable device string for alsasrc
        if is_src_device:
            devices = os.popen("arecord -l")
        else:
            devices = os.popen("aplay -l")
        device_strings = devices.read().split("\n")
        # The entry for a capture device starts with 'card n' where n is an integer
        # The syntax for refering to the capture device is 'hw:n' with the n from above
        device_list = [
            ("hw:" + s[len("card ") :]).partition(": ")[0::2]
            for s in device_strings
            if s.startswith("card ")
            # filter out the headphone jack as it could cause serious problems with the Pi's clk
            and "Headphones [bcm2835 Headphones], device 0: bcm2835 Headphones [bcm2835 Headphones]"
            not in s
        ]
        return device_list

    def get_audio_device_names(self, is_src_device: bool):
        return [s[1] for s in self.get_audio_devices(is_src_device)]

    def get_audio_device_dev_by_name(self, name, is_src_device: bool):
        ret = [a[0] for a in self.get_audio_devices(is_src_device) if a[1] == name]
        # Should get exactly one match
        if not len(ret):
            logger.warning(
                "No matching audio {0} device, using one of the available devices according to arcane rules".format(
                    "src" if is_src_device else "sink"
                )
            )
            return self.get_audio_device(is_src_device)
        if len(ret) > 1:
            logger.warning(
                "More than one audio {0} device matched the selected string, using first match".format(
                    "src" if is_src_device else "sink"
                )
            )
        return ret[0]

    def get_audio_device_name_by_dev(self, dev, is_src_device: bool):
        ret = [a[1] for a in self.get_audio_devices(is_src_device) if a[0] == dev]
        # Should get exactly one match
        if not len(ret):
            logger.warning(
                "No matching audio {0} device, using one of the available devices according to arcane rules".format(
                    "src" if is_src_device else "sink"
                )
            )
            # FIXME: This is wrong and should never happen unless no device is selected or a device disappeared
            return ""
        if len(ret) > 1:
            logger.warning(
                "More than one audio {0} device matched the selected string, using first match".format(
                    "src" if is_src_device else "sink"
                )
            )
        return ret[0]

    def get_audio_device(self, is_src_device: bool):
        # Pick the first audio input device
        devs = self.get_audio_devices(is_src_device)
        if devs:
            return devs[0][0]
        return ""

    """
    VIDEO DEVICE SECTION
    """

    def get_video_device(self):
        # return VideoDeviceModel.TEST, ""
        devices = os.popen("v4l2-ctl --list-devices")
        device_string = devices.read()
        device_string = device_string.split("\n")
        ignore_videosources = self.app.settings["StreamingDirector.ignore_videosources"]
        if ignore_videosources != []:
            logger.info(f"Preparing to ignore video sources matching '{ignore_videosources}'")
        for x in range(len(device_string)):
            try:
                video_device = device_string[x + 1].strip()
            except:
                return VideoDeviceModel.TEST, ""
            # Check all possible matches in the ignorelist
            if [vs for vs in ignore_videosources if vs in device_string[x]]:
                logger.info(f"Ignoring the video source '{device_string[x]}' matched by '{ignore_videosources}'")
                continue
            if device_string[x].find("Hauppauge USB Live 2") != -1:
                return VideoDeviceModel.HAUPPAUGE, video_device
            elif device_string[x].find("mmal service 16.1 (platform:bcm2835-v4l2") != -1:
                return VideoDeviceModel.RASPICAM, video_device
            elif device_string[x].find("unicam (platform:fe801000.csi)") != -1:
                return VideoDeviceModel.HDMI2CSI, video_device
            elif device_string[x].find("Cam Link 4K: Cam Link 4K") != -1:
                return VideoDeviceModel.CAMLINK, video_device
            elif device_string[x].find("534d:2109") != -1:
                # Macrosilicon USB 2.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("USB3.0 HD VIDEO") != -1:
                # Macrosilicon USB 2.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("USB Video: USB Video") != -1:
                # Macrosilicon USB 2.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("usb video: usb video") != -1:
                # Macrosilicon USB 2.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("USB3.0 Capture: USB3.0 Capture") != -1:
                # Macrosilicon USB 3.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("USB3. 0 capture: USB3. 0 captur") != -1:
                # Macrosilicon USB 3.0 HDMI Capture Device
                return VideoDeviceModel.MACROSILICON2109, video_device
            elif device_string[x].find("PTZ Pro 2") != -1:
                return VideoDeviceModel.LOGITECHPTZPRO2, video_device
            elif device_string[x].find("Logitech MeetUp") != -1:
                return VideoDeviceModel.LOGITECHMEETUP, video_device
            elif device_string[x].find("Blackmagic Design") != -1:
                # Blackmagic Design Atem Mini in lots of variants
                # Output always fixed to 1080p
                return VideoDeviceModel.ATEMMINIPRO, video_device
            elif device_string[x].find("Web Presenter HD:") != -1:
                # Blackmagic Design Web Presenter
                return VideoDeviceModel.ATEMMINIPRO, video_device
            elif device_string[x].find("ATEM 1 M/E Constellation HD:") != -1:
                # Blackmagic Design ATEM 1 M/E Constellation HD
                # Output in a single configured resolution
                return VideoDeviceModel.ATEMMINIPRO, video_device
        return VideoDeviceModel.TEST, ""

    # # differentiate between spare/local PIs and cloud PIs
    # def start_pipeline_user(self, receive_pipe, test=False):
    #     if self.app.settings["Device.device_type"] == "spare":
    #         self.start_pipeline(None, receive_pipe)
    #     else:
    #         if receive_pipe:
    #             key_pressed = "start_receive"
    #         else:
    #             if not test:
    #                 key_pressed = "start_stream"
    #             else:
    #                 key_pressed = "start_test"
    #         r = self.cloud_request.post(
    #             self.app.settings["PortalDirector.keypress_url"],
    #             data={"key": key_pressed},
    #         )
    #         logger.info(r.json())

    def start_pipeline(
        self, stream_url=None, receive_pipe=False, recover_from_error=False
    ):
        # if self.self.receive_pipe == ErrorStates.NULL:
        if not receive_pipe:
            video_device_model, video_dev = self.get_video_device()
            audio_dev = self.get_audio_device(True)

            if video_dev == VideoDeviceModel.TEST or not audio_dev:
                logger.error("no also or video device found -> teststream")
                logger.info(f"video_dev: {video_dev}")
                logger.info(f"audio_dev {audio_dev}")
                if recover_from_error:
                    # the audio/vid interface might be dead so we can't find it
                    # so we throw an error here
                    # TODO: return error
                    video_device_model = VideoDeviceModel.CAMLINK
                    audio_dev = ""
                else:
                    video_device_model = VideoDeviceModel.TEST
                    audio_dev = ""

            # forcing the pipe if the config is set to
            force_pipe = self.app.settings["StreamingDirector.force_pipe"]
            if force_pipe != "None":
                logger.info(f"Forcing the video pipe to {force_pipe}")
                video_device_model = VideoDeviceModel[force_pipe]

            # choose the actually used pipe (string representation)
            if video_device_model == VideoDeviceModel.HAUPPAUGE:
                used_pipe = PIPELINE_DESC_ANALOG
            elif video_device_model == VideoDeviceModel.RASPICAM:
                used_pipe = PIPELINE_DESC_RASPICAM
            elif video_device_model == VideoDeviceModel.HDMI2CSI:
                used_pipe = PIPELINE_DESC_HDMI2CSI
            elif video_device_model == VideoDeviceModel.CAMLINK:
                used_pipe = PIPELINE_DESC_CAMLINK
            elif video_device_model == VideoDeviceModel.ATEMMINIPRO:
                used_pipe = PIPELINE_DESC_ATEMMINIPRO
            elif video_device_model == VideoDeviceModel.MACROSILICON2109:
                used_pipe = PIPELINE_DESC_MACROSILICON2109
            elif video_device_model == VideoDeviceModel.LOGITECHPTZPRO2:
                used_pipe = PIPELINE_DESC_LOGITECHPTZPRO2
            elif video_device_model == VideoDeviceModel.LOGITECHMEETUP:
                used_pipe = PIPELINE_DESC_LOGITECHMEETUP
            elif video_device_model == VideoDeviceModel.TEST:
                logger.info("Test video device selected")
                used_pipe = PIPELINE_DESC_TEST
            else:
                logger.error("Unknown video device type")
                used_pipe = PIPELINE_DESC_TEST

            logger.debug("Used Pipe: {}".format(used_pipe))
            self.pipe = Gst.parse_launch(used_pipe)

            # fmt: off
            self.set_property_obj_and_load_persistence("extra_controls",         self.pipe.get_by_name('encoder'))
            self.set_property_obj_and_load_persistence("video_bitrate",          self.pipe.get_by_name('encoder'))
            self.set_property_obj_and_load_persistence("volume",                 self.pipe.get_by_name('volume'))
            self.set_property_obj_and_load_persistence("rtmp_location",          self.pipe.get_by_name('rtmp_sink'))
            self.set_property_obj_and_load_persistence("wb_red",                 self.pipe.get_by_name('wb'))
            self.set_property_obj_and_load_persistence("wb_blue",                self.pipe.get_by_name('wb'))
            self.set_property_obj_and_load_persistence("wb_green",               self.pipe.get_by_name('wb'))
            self.set_property_obj_and_load_persistence("image_overlay_location", self.pipe.get_by_name('image_overlay'))
            self.set_property_obj_and_load_persistence("audio_source_device",    self.pipe.get_by_name('audio_src'))
            # fmt: on

            self.metrics_txqueue = self.pipe.get_by_name("sink_queue")

            # setting the rtmp streaming key/location
            location_add = ""
            if video_device_model == VideoDeviceModel.HAUPPAUGE:
                location_add = "43"
            if stream_url is None:
                location = (
                    self.app.settings["StreamingDirector.streaming_url"]
                    + location_add
                    + "/"
                    + self.hostname
                    + self.app.settings["StreamingDirector.streaming_key"]
                )
            else:
                location = stream_url
            # self.rtmp_sink.set_property("location", location)
            self.set_property("rtmp_location", location)
            logger.info(f"set rtmp location to: {location}")

            # print the audio source device
            audio_src = self.pipe.get_by_name("audio_src")
            if audio_src:
                audio_dev = audio_src.get_property("device")
                audio_dev_name = self.get_audio_device_name_by_dev(audio_dev, True)
                logger.info(f"using audio src device: {audio_dev} \"{audio_dev_name}\"")

            # setting the video src device
            video_src = self.pipe.get_by_name("video_src")
            if video_src:
                logger.info(f"using video device: {video_dev}")
                logger.info(f"video type: {video_device_model.value}")
                video_src.set_property("device", video_dev)

        else:  # Receive Pipe
            used_pipe = PIPELINE_DESC_RECEIVE
            self.pat_counter = 0
            self.pmt_counter = 0

            logger.debug("Used Pipe: {}".format(used_pipe))
            self.pipe = Gst.parse_launch(used_pipe)

            self.set_property_obj_and_load_persistence(
                "rtmp_location", self.pipe.get_by_name("video_src")
            )
            self.set_property_obj_and_load_persistence(
                "audio_sink_device", self.pipe.get_by_name("audio_sink")
            )

            if stream_url is not None:
                self.set_property("rtmp_location", stream_url)
                logger.info(f"set rtmp location to: {stream_url}")
            # self.metrics_txqueue = self.pipe.get_by_name("sink_queue")

            # print the audio sink device
            audio_sink = self.pipe.get_by_name("audio_sink")
            if audio_sink:
                audio_dev = audio_sink.get_property("device")
                audio_dev_name = self.get_audio_device_name_by_dev(audio_dev, False)
                logger.info(f"using audio sink device: {audio_dev} \"{audio_dev_name}\"")

        bus = self.pipe.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)

        self.pipe.set_state(Gst.State.PLAYING)
        # self.state_intended = ErrorStates.PLAYING

        self.receive_pipe = receive_pipe

        # self.app.settings["Device.stream_started"] = True
        # self.app.settings["Device.receive_pipe"] = self.receive_pipe

    # # differentiate between spare/local PIs and cloud PIs
    # # Todo: make it async
    # def stop_pipeline_user(self):
    #     if self.app.settings["Device.device_type"] == "spare":
    #         self.stop_pipeline()
    #     else:
    #         if self.receive_pipe:
    #             key_pressed = "stop_receive"
    #         else:
    #             key_pressed = "stop_stream"
    #         r = self.cloud_request.post(
    #             self.app.settings["PortalDirector.keypress_url"],
    #             data={"key": key_pressed},
    #         )
    #         logger.info(r.json())
    #         self.stop_pipeline()

    def stop_pipeline(self):
        # if self.state_intended != ErrorStates.NULL:
        # self.app.overlay_generator.generate_end_card()
        # time.sleep(2)

        self.pipe.set_state(Gst.State.NULL)
        # self.state = ErrorStates.NULL
        # self.state_intended = ErrorStates.NULL
        self.deregister_all_property_obj()
        self.pipe = None
        self.metrics_txqueue = None
        self.metrics_txqueue_bitrate.set(0)
        self.metrics_txqueue_bytes.set(0)
        self.metrics_txqueue_buffers.set(0)
        for i in range(self.metrics_audiolevel_channel_count):
            self.metrics_audiolevel.labels(i, 'rms').set(self.METRICS_AUDIOLEVEL_DEFAULT)
            self.metrics_audiolevel.labels(i, 'peak').set(self.METRICS_AUDIOLEVEL_DEFAULT)
            self.metrics_audiolevel.labels(i, 'decay').set(self.METRICS_AUDIOLEVEL_DEFAULT)

    def update_stream_location(self, url):
        # @TODO: we might need a more sophisticated method here (stop the rtmpsink only rather then the whole pipe)
        ret = False
        # if self.state_intended == ErrorStates.PLAYING:
        self.pipe.set_state(Gst.State.NULL)
        self.set_property("rtmp_location", url)
        self.pipe.set_state(Gst.State.PLAYING)
        ret = True

        return ret

    def set_property_obj_and_load_persistence(self, property_name, obj):

        if not property_name in self.properties:
            logger.error(f"property '{property_name}' not available in properties, we only have '{self.properties.keys()}'")
            return

        self.properties[property_name]["obj"] = obj

        if self.properties[property_name]["local_config_persistent"]:
            self.set_property(
                property_name,
                self.app.settings[f"StreamingDirector.{property_name}"],
            )
            # if "properties" in self.config_local:
            #     if property_name in self.config_local["properties"]:
            #         self.set_property(
            #             property_name,
            #             self.config_local["properties"][property_name],
            #             False,
            #         )

    def register_property(
        self,
        property_name,
        gst_property_name,
        local_config_persistent,
        value_lambda,
        value_reverse_lambda,
    ):
        if not (property_name or gst_property_name):
            logger.error("register_property false input")
            logger.error(property_name)
            logger.error(gst_property_name)
            return

        logger.info(
            f"register_property: {property_name} {gst_property_name} {type(value_lambda)} {type(value_reverse_lambda)}"
        )
        self.properties[property_name] = {
            "obj": None,
            "gst_property_name": gst_property_name,
            "local_config_persistent": local_config_persistent,
            "value_lambda": value_lambda,
            "value_reverse_lambda": value_reverse_lambda,
        }

    def deregister_all_property_obj(self):
        logger.info("deregister all objects in properties")
        for _, p in self.properties.items():
            p["obj"] = None

    def property_set_callback(self, key, value):
        logger.debug(f"callback property change '{key}' '{value}'")
        return self.set_property(key, value)

    def set_property(self, property_name, value, write=True):
        if not ((property_name in self.properties)):
            logger.warning(f"requested property '{property_name}' not registered, we only have '{self.properties.keys()}'")
            return "false"

        # safe the property in the config file
        # if (self.properties[property_name]["local_config_persistent"]) & (
        #     write
        # ):
        #     self.config_local["properties"][property_name] = value
        #     self.config_local_write()

        # set the property on a running stream if the stream is currently active
        if self.properties[property_name]["obj"]:
            # log the value before it is transformed into machine-readable form
            logger.info(f"set property '{property_name}' to '{value}'")
            # call the value_lambda function on the actual value
            if self.properties[property_name]["value_lambda"]:
                value = self.properties[property_name]["value_lambda"](value)

            # set property
            obj = self.properties[property_name]["obj"]
            gst_property_name = self.properties[property_name]["gst_property_name"]
            obj.set_property(gst_property_name, value)
            return "true"

        return "true"

    def get_property(self, property_name):

        ret = "false"
        # first try to get the value from the acutal stream
        # if this is not successfull then try to get it from the persistence saves
        if not ((property_name in self.properties)):
            logger.error(f"requested property '{property_name}' not registered, we only have '{self.properties.keys()}'")
        elif self.properties[property_name]["obj"]:
            obj = self.properties[property_name]["obj"]
            gst_property_name = self.properties[property_name]["gst_property_name"]
            ret = obj.get_property(gst_property_name)
            # call the value_reverse_lambda function on the actual value
            if self.properties[property_name]["value_reverse_lambda"]:
                ret = self.properties[property_name]["value_reverse_lambda"](ret)
        # elif self.properties[property_name]["local_config_persistent"]:
        #     ret = self.config_local.get("properties", property_name, fallback="false")
        #     # call the value_reverse_lambda function on the actual value
        #     if self.properties[property_name]["value_reverse_lambda"] :
        #         ret = self.properties[property_name]["value_reverse_lambda"](ret)

        return ret

    async def publish_metrics(self):
        while True:
            await asyncio.sleep(5)
            logger.debug("publish metrics")
            if self.metrics_txqueue:
                q = self.metrics_txqueue.get_property("avg-in-rate")
                self.metrics_txqueue_bitrate.set(q * 8)
                q = self.metrics_txqueue.get_property("current-level-bytes")
                self.metrics_txqueue_bytes.set(q)
                q = self.metrics_txqueue.get_property("current-level-buffers")
                self.metrics_txqueue_buffers.set(q)
                # print("publish metrics")
                # print(q)

    def on_message(self, bus, message):
        t = message.type

        printable_messages = {
            Gst.MessageType.ERROR,
            Gst.MessageType.WARNING,
            Gst.MessageType.INFO,
            Gst.MessageType.EOS,
            Gst.MessageType.STREAM_START,
            Gst.MessageType.STATE_CHANGED,
            Gst.MessageType.QOS,
            Gst.MessageType.BUFFERING,
            # Gst.MessageType.TAG,
            # Gst.MessageType.STREAM_STATUS,
            # Gst.MessageType.ELEMENT,
            "other"
        }
        if t == Gst.MessageType.ERROR:
            # self.player.set_state(Gst.State.NULL)
            # self.state = ErrorStates.ERROR

            err, debug = message.parse_error()
            errordetails = message.parse_error_details()
            if errordetails:
                errordetails = errordetails.to_string()
            else:
                errordetails = "None"
            if t in printable_messages:
                logger.info(
                    "Error message from %s: %s, associated debug info: %s, details: %s"
                    % (message.src.name, err, debug, errordetails)
                )
            if message.src.name.startswith("video_src"):
                if "not-negotiated" in debug:
                    if hasattr(self, "pipe"):
                        if self.pipe:
                            vsrc = self.pipe.get_by_name("video_src")
                            if vsrc:
                                pad = vsrc.get_static_pad("src")
                                if pad:
                                    caps = pad.get_current_caps()
                                    if not caps:
                                        caps = pad.query_caps(None)
                                    if caps:
                                        logger.info(f"caps of video_src is {caps.to_string()}")

            #
            if message.src.name.startswith("rtmp_sink"):
                self.app.main_director.send_message(
                    MainDirectorMessages.ERROR_NETWORK, errordetails
                )
            # hlsdemux throws the error if the hls playlist is empty or not updated (e.g. if the stream has finished)
            elif message.src.name.startswith("hls_demux"):
                self.app.main_director.send_message(
                    MainDirectorMessages.ERROR_NETWORK, errordetails
                )
            else:
                self.app.main_director.send_message(MainDirectorMessages.ERROR)
        elif t == Gst.MessageType.WARNING:
            if t in printable_messages:
                logger.info(
                    "Warning message from %s: %s"
                    % (message.src.name, message.parse_warning())
                )

            if message.src.name.startswith("video_src"):
                warning_details, _ = message.parse_warning()
                logger.warning(f"warning details code: '{warning_details.code}' domain: '{warning_details.domain}' message: '{warning_details.message}'")
                if warning_details.message in ["Signal lost"]:
                    # occurs on analog haupauge grabber when no signal is there (blue picture)
                    logger.warn("Signal lost")

                    # NO restart required
                else:
                    self.app.main_director.send_message(
                        MainDirectorMessages.ERROR_DEVICE, message.parse_warning()
                    )

            elif message.src.name.startswith("glimagesink"):
                warning_details, _ = message.parse_warning()
                logger.warning(f"warning details code: '{warning_details.code}' domain: '{warning_details.domain}' message: '{warning_details.message}'")
                if warning_details.message in ["A lot of buffers are being dropped."] and self.receive_pipe == True:
                    logger.warn("receiving decoder probably stuck so reporting an NETWORK ERROR")
                    self.app.main_director.send_message(
                        MainDirectorMessages.ERROR_NETWORK, message.parse_warning()
                    )


        elif t == Gst.MessageType.INFO:
            if t in printable_messages:
                logger.info(
                    "Info message from %s: %s" % (message.src.name, message.parse())
                )
        elif t == Gst.MessageType.EOS:
            # self.player.set_state(Gst.State.NULL)
            if t in printable_messages:
                logger.info("EOS message from %s" % message.src.name)
        elif t == Gst.MessageType.STREAM_START:
            # self.player.set_state(Gst.State.NULL)
            if t in printable_messages:
                logger.info("Stream start message from %s" % message.src.name)
        elif t == Gst.MessageType.STATE_CHANGED:
            old_state, new_state, _ = message.parse_state_changed()
            if (
                t in printable_messages
                or (
                    message.src.name
                    in ("video_src", "audio_src", "encoder", "rtmp_sink")
                )
                or (message.src.name.startswith("pipeline"))
            ):
                logger.debug(
                    "StateChange message from %s: %s -> %s"
                    % (message.src.name, old_state.value_nick, new_state.value_nick)
                )
            if message.src.get_name().startswith("pipeline"):
                if new_state.value_nick == "null":
                    # self.state = ErrorStates.NULL
                    self.app.main_director.send_message(
                        MainDirectorMessages.STREAM_STOPPED
                    )
                elif new_state.value_nick == "ready":
                    # self.state = ErrorStates.READY
                    self.app.main_director.send_message(
                        MainDirectorMessages.STREAM_STARTING
                    )
                elif new_state.value_nick == "paused":
                    # self.state = ErrorStates.PAUSED
                    self.app.main_director.send_message(
                        MainDirectorMessages.STREAM_STARTING
                    )
                elif new_state.value_nick == "playing":
                    # self.state = ErrorStates.PLAYING
                    self.app.main_director.send_message(
                        MainDirectorMessages.STREAM_PLAYING
                    )
                else:
                    # self.state = ErrorStates.ERROR
                    self.app.main_director.send_message(MainDirectorMessages.ERROR)
        elif t == Gst.MessageType.QOS:
            if t in printable_messages:
                logger.info(
                    "QoS Message from %s: %s" % (message.src.name, message.parse_qos())
                )

            # if we are receiving and glimagesink drops us some QoS message than usaually s.th. is broaken
            if message.src.name.startswith("glimagesink") and self.receive_pipe == True:
                logger.warn("QoS from glimagesink: receiving decoder probably stucked so reporting an NETWORK ERROR")
                self.app.main_director.send_message(
                    MainDirectorMessages.ERROR_NETWORK, message.parse_qos()
                )

        elif t == Gst.MessageType.STREAM_STATUS:
            if t in printable_messages:
                logger.info(
                    "Stream status message from %s: %s"
                    % (message.src.name, message.parse_stream_status())
                )
        elif t == Gst.MessageType.NEED_CONTEXT:
            if t in printable_messages:
                logger.info(
                    "Need_context message from %s: %s"
                    % (message.src.name, message.parse_context_type())
                )
        elif t == Gst.MessageType.HAVE_CONTEXT:
            if t in printable_messages:
                logger.info(
                    "Have_context message from %s: %s"
                    % (message.src.name, message.parse_have_context())
                )
        elif t == Gst.MessageType.TAG:
            if t in printable_messages:
                logger.debug(
                    "TAG message from %s: %s"
                    % (message.src.name, message.parse_tag())
                )
        # elif t == Gst.MessageType.DURATION_CHANGED:
        #     (ret1, duration1) = self.pipe.query_duration(Gst.Format.TIME)
        #     (ret2, duration2) = self.pipe.get_by_name("video_src").query_duration(Gst.Format.TIME)
        #     logger.debug(
        #         f"DURATION_CHANGED message from {message.src.name}: {ret1} {duration1}"
        #     )
        #     logger.debug(
        #         f"DURATION_CHANGED message from {message.src.name}: {ret2} {duration2}"
        #     )
        elif t == Gst.MessageType.ELEMENT:
            message_struct = message.get_structure()
            if t in printable_messages:
                logger.debug(
                    "ELEMENT message from %s: %s"
                    % (message.src.name, message_struct.to_string())
                )
            if message_struct.get_name() in ["pat"]:
                self.pat_counter = self.pat_counter + 1
                logger.info(f"pat_counter: {self.pat_counter}")

            if message_struct.get_name() in ["pmt"]:
                self.pmt_counter = self.pmt_counter + 1
                logger.info(f"pmt_counter: {self.pmt_counter}")
                # if self.pmt_counter > 1:
                #     self.app.main_director.send_message(
                #         MainDirectorMessages.ERROR_NETWORK, f"pmt counter too high {self.pmt_counter}"
                #     )

            # log which is the next file / TS chunk
            if (
                message.src.name.startswith("souphttpsrc")
                and message_struct.get_value("uri")
                and self.receive_pipe == True
            ):
                file_name = os.path.basename(message_struct.get_value("uri"))
                logger.info(f"next downloaded file: {file_name}")

            if (
                message.src.name.startswith("hls_demux")
                and message_struct.get_value("fragment-start-time")
                and message_struct.get_value("fragment-stop-time")
                and message_struct.get_value("fragment-download-time")
            ):
                # FIXME: GStreamer uses different time units for these messages.
                # That's a GStreamer bug.
                # fragment-start-time uses microseconds instead of nanoseconds.
                # The offending code is in gstadaptivedemux.c where time handling
                # is a horrible mix of microseconds and nanoseconds.
                # The new adaptivedemux2 in GStreamer 1.22 doesn't have any stats.
                gst_broken_timebase_fragment_start_time_to_nsecond = 1000
                frag_starttime = int(message_struct.get_value("fragment-start-time"))
                frag_starttime *= gst_broken_timebase_fragment_start_time_to_nsecond
                frag_stoptime = int(message_struct.get_value("fragment-stop-time"))
                frag_downtime = int(message_struct.get_value("fragment-download-time"))
                # Interestingly fragment-download-time and the difference between
                # fragment-stop-time and fragment-start-time are not identical.
                # Apparently, fragment-download-time does not include some overhead.
                # Until we understand this better, report the longer of both times.
                frag_difftime = frag_stoptime - frag_starttime
                frag_overhead = frag_difftime - frag_downtime
                # Convert reported times to milliseconds
                frag_difftime //= Gst.MSECOND
                frag_overhead //= Gst.MSECOND
                file_name = os.path.basename(message_struct.get_value("uri"))
                logger.debug(
                    f"Download of {file_name} took {frag_difftime} ms ({frag_overhead} ms overhead)"
                )

            # update the audiolevel metrics
            if message.src.name in ["audiolevel"]:
                if message_struct.get_name() in ["level"]:
                    logger.debug(f"audiolevel array {message_struct.get_value('rms')}")
                    if self.metrics_audiolevel:
                        rms = message_struct.get_value('rms')
                        peak = message_struct.get_value('peak')
                        decay = message_struct.get_value('decay')

                        if len(rms) != len(peak) or len(rms) != len(decay):
                            raise ValueError('Conflicting number of audio channels in level message')
                            
                        self.metrics_audiolevel_channel_count = len(rms)
                        logger.debug(f"count {self.metrics_audiolevel_channel_count}")

                        for index in range(self.metrics_audiolevel_channel_count):
                            self.metrics_audiolevel.labels(index, 'rms').set(rms[index])
                            self.metrics_audiolevel.labels(index, 'peak').set(peak[index])
                            self.metrics_audiolevel.labels(index, 'decay').set(decay[index])
        elif t == Gst.MessageType.BUFFERING:
            if t in printable_messages:
                #logger.debug(
                #    "BUFFERING message from %s: %s"
                #    % (message.src.name, message.parse_buffering())
                #)
                buffering_queue = self.pipe.get_by_name(message.src.name)
                if buffering_queue:
                    level_bytes = buffering_queue.get_property("current-level-bytes")
                    level_buffers = buffering_queue.get_property("current-level-buffers")
                    level_time_ms = buffering_queue.get_property("current-level-time") // Gst.MSECOND
                    avg_in_rate = buffering_queue.get_property("avg-in-rate")
                    logger.debug(f"{message.src.name} buffer: {level_bytes} bytes, {level_buffers} buffers, {level_time_ms} ms, {avg_in_rate} B/s avg-in-rate")
                else:
                    logger.debug(f"BUFFERING message from {message.src.name}, but the element does not exist anymore")
        else:
            if "other" in printable_messages:
                logger.debug(f"Other message {t} from {message.src.name}")
