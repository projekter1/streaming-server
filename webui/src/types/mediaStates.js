
const mediaStates = {
  NULL: 'NULL',
  READY: 'READY',
  STARTING: 'STARTING',
  PLAYING: 'PLAYING',
  ERROR: 'ERROR',
  RECOVER: 'RECOVER',
  WS_CONNECTION_LOST: 'WS_CONNECTION_LOST'

}

export default mediaStates
