/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2023 Stephan Strittmatter
*/
import axios from 'axios'
import api from './api'

const apiinfoService = {

  async getInfo () {
    const response = await this.restClient().get('openapi.json')
    if (response.status === 200) {
      console.log(response.data?.info)
      return response.data?.info
    }
    console.error(response)
  },
  restClient () {
    return axios.create({
      baseURL: api.getServer(),
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}

export default apiinfoService
