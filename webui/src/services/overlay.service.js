/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import api from './api'

const overlayService = {
  async getImagesList () {
    const response = await api.restClient().get('/overlay/file')

    if (response.status === 200 && response.data.return.msg === 'OK') {
      let sortedList = response.data.return.data.pictures.sort((a, b) => a.filename < b.filename ? -1 : 1)
      sortedList = sortedList.map(item => ({
        filename: item.filename,
        fixed: item.fixed,
        href: `${api.getServer()}/overlayimages/${item.filename}`
      }))
      return sortedList
    }
    console.error('loading overlay images failed', response)
  },
  async addNewOverlayImage (formData) {
    const config = {
      headers: { 'content-type': 'multipart/form-data' }
    }
    const response = await api.restClient().post('/overlay/file', formData, config)

    return response
  },
  async deleteOverlayImage (file) {
    const params = new URLSearchParams({ filename: file })
    const response = await api.restClient().delete(`/overlay/file?${params}`)
    return response
  },
  async setImageOverlay (file) {
    const params = new URLSearchParams({ path: file })
    const response = await api.restClient().post(`/overlay/image?${params}`)
    if (response.status !== 200) {
      console.error('setting image overlay failed', response)
    }
  },
  async clearOverlayImage () {
    // TODO axios.delete(`${api.getApi()}/overlay/overlay_image`)
    await api.restClient().delete('/overlay/image')
  },
  getSupportedImageFormat () {
    return [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  },
  async setHymnOverlay (hymn, strophe) {
    const params = new URLSearchParams({ hymn, verse: strophe })
    const response = await api.restClient().post(`/overlay/hymn?${params}`)
    if (response.status !== 200) {
      console.error('setting image overlay failed', response)
    } else if (response.data.return.rc === 1) {
      console.error(response.data.return.msg)
    }
  },
  async setTextOverlay (left, right) {
    const params = new URLSearchParams({ left, right })
    const response = await api.restClient().post(`/overlay/text?${params}`)
    if (response.status === 200) {
      return true
    }
    console.error('setting text overlay failed', response)
  },
  async clearTextOverlay () {
    const response = await api.restClient().delete('/overlay/text')
    if (response.status !== 200) {
      console.error('clearing texte overlay failed', response)
    }
  }
}

export default overlayService
