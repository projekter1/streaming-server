/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2022 Tobias Pape
*/
import axios from 'axios'

const API_PREFIX = '/api/v2'

const api = {
  server: null,

  /**
   * Get the base-URL of the Service
   */
  getServer () {
    if (this.server) {
      return this.server
    }
    if (process.env.VUE_APP_HAIEND_STREAMING_SERVER) {
      this.server = process.env.VUE_APP_HAIEND_STREAMING_SERVER
    } else {
      this.server = window.location.origin
      if (this.server.charAt(this.server.length - 1) === '/') {
        this.server = this.server.substring(0, this.server.length - 1)
      }
    }
    console.info(`%c[Hai-End-Streaming] ⚙️ Rest-Service: ${this.server}`, 'color: #5793c9')
    return this.server
  },
  getApi () {
    return this.getServer() + API_PREFIX
  },
  restClient () {
    return axios.create({
      baseURL: this.getApi(),
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}

export default api
