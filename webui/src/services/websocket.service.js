/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2022 Stephan Strittmatter
*/

const websocketService = {
  server: null,

  getServer () {
    if (this.server) {
      return this.server
    }

    const url = '/api/v2/ws/'
    if (process.env.VUE_APP_WEBSOCKET_SERVER) {
      this.server = `${process.env.VUE_APP_WEBSOCKET_SERVER}${url}`
    } else {
      this.server = `ws://${window.location.host}${url}`
    }
    console.info(`%c[Hai-End-Streaming] 🔌 Websocket-Service:  ${this.server}`, 'color: #5793c9')
    return this.server
  }
}

export default websocketService
