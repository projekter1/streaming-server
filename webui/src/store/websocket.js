/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2022 Stephan Strittmatter
  Copyright (C) 2024 Carl-Daniel Hailfinger
*/

import { defineStore } from 'pinia'
import { useStreamingStore } from './streaming'
import mediaStates from '@/types/mediaStates'
import websocketService from '../services/websocket.service'

// export const namespaced = false // have to be false!
export const useWebsocketStore = defineStore('websocket', {
  state: () => ({
    connection: null
  }),
  actions: {
    openConnection () {
      if (!this.connection) {
        this.connection = new WebSocket(websocketService.getServer())
        this.connection.onopen = (event) => {
          this.SOCKET_ONOPEN(event)
        }
        this.connection.onreconnect = (event) => {
          this.SOCKET_RECONNECT(event)
        }
        this.connection.onclose = (event) => {
          this.SOCKET_ONCLOSE(event)
        }
        this.connection.onreconnecterror = (event) => {
          this.SOCKET_RECONNECT_ERROR(event)
        }
        this.connection.onerror = (event) => {
          this.SOCKET_ONERROR(event)
        }
        this.connection.onmessage = (event) => {
          this.SOCKET_ONMESSAGE(event)
        }
      }
      return this.connection
    },
    reConnect () {
      this.connection = null
      console.info('Websocket: Reconnect will be attempted in 3 second.')
      setTimeout(() => {
        this.openConnection()
      }, 3000)
    },
    SOCKET_ONOPEN (payload) {
      console.info('SOCKET_ONOPEN', payload)
      console.log('successful websocket connection')
    },
    SOCKET_RECONNECT (payload) {
      console.info('SOCKET_RECONNECT', payload)
    },
    SOCKET_ONCLOSE (payload) {
      console.info('SOCKET_ONCLOSE', payload)
      const streaming = useStreamingStore()
      streaming.setServerStatus({
        streaming: {
          current: mediaStates.WS_CONNECTION_LOST
        }
      })
      this.reConnect()
    },

    SOCKET_ONERROR (payload) {
      console.error('SOCKET_ONERROR', payload)
      const streaming = useStreamingStore()
      streaming.setServerStatus({
        streaming: {
          current: mediaStates.WS_CONNECTION_LOST
        }
      })
      this.reConnect()
    },

    SOCKET_ONMESSAGE (payload) {
      // dispatch to specific store
      const data = JSON.parse(payload.data)
      if (data.state) {
        const streaming = useStreamingStore()
        streaming.setServerStatus(data.state)
      } else if (data.server) {
        if (data.server.imagetype && (data.server.imagetype !== 'production')) {
          console.log('Server imagetype changed, forcing reload')
          window.location.reload(true)
        }
        // TODO: Check the server version as well, but allow mismatch for
        // development environments
      } else if (data.tests) {
        // data.tests is only present for recovery imagetype
        // This is a heuristic in absence of a data.server.imagetype
        console.log('Server imagetype changed to recovery, forcing reload')
        window.location.reload(true)
      } else {
        console.warn('SOCKET_ONMESSAGE: Unknown websocket message', payload)
      }
    },
    SOCKET_RECONNECT_ERROR () {
      console.error('Reconnect Websocket error')
      this.reConnect()
    }
  }
})
