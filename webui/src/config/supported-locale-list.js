/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
export default [
  {
    code: 'en',
    countryCode: 'gb',
    name: 'English'
  },
  {
    code: 'de',
    countryCode: 'de',
    name: 'Deutsch'
  },
  {
    code: 'fr',
    countryCode: 'fr',
    name: 'Français'
  },
  {
    code: 'nl',
    countryCode: 'nl',
    name: 'Nederlands'
  }
]
